
describe("Mongoose", function () {

    var when ,now;
    now = when = require('../lib/execute')();

    var should = require('should');
    var mongoose = require('mongoose');

    after(function (done) {
        mongoose.connection.db.dropDatabase(function (err, result) {
            mongoose.connection.close();
            done();
        });
    });

    it("should successfully opened", function (done) {
        mongoose.connect('mongodb://localhost/koi-test');
        var db = mongoose.connection;

        db.on('error', function (err) { });

        db.once('open', function () {

            var testSchema = mongoose.Schema({
                tid: String
            });

            Test = mongoose.model('Test', testSchema);

            done();
            now('ready');
        });
    });

    var Test;

    it("should trigger ready state on execute.js", function (done) {
        when('ready', done);
    });

    it("should return null when no matching found for schema", function (done) {
        when('ready', function () {
            Test.findOne({tid: "foo"})
            .exec(function (err, test) {
                should.not.exist(err);
                should.strictEqual(test, null);
                done();
            });
        });
    });

    it("should return undefined test when error", function (done) {
        when('ready', function () {
            Test.findOne({_id: "id"})
            .exec(function (err, test) {
                should.exist(err);
                should.strictEqual(test, undefined);
                done();
            });
        });
    });

});
