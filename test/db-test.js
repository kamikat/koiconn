
describe("logic module", function () {

    var should = require('should');
    var logic = require('../lib/db');

    logic.connect('mongodb://localhost/koi-test');

    after(function (done) {
        logic.mongoose.connection.db.dropDatabase(function (err, result) {
            logic.close();
            done();
        });
    });

    it('should return user whose uid is not in database', function(done) {
        logic.getUser("foo", function (err, user) {
            should.not.exist(err);
            should.exist(user);
            user.should.have.property('uid');
            user.should.not.have.property('name');
            done();
        });
    });

    it('should update user profile from uid', function(done) {
        logic.getUser("foo", function (err, user) {
            should.not.exist(err);
            should.exist(user);
            logic.updateUserProfile(user, {
                name: "Jason",
                avatar: ["about:blank", "http://foo.bar.com/test.jpg", "invalid"]
            }, function (err, user) {
                should.not.exist(err);
                should.exist(user);
                user.should.have.property('uid');
                user.should.have.property('name');
                user.should.have.property('last_updated');
                done();
            });
        });
    });

    it('should update user object after created a new match', function(done) {
        logic.getUser("foo", function (err, user) {
            should.not.exist(err);
            should.exist(user);
            logic.newMatch(user, {
                body: "Long Life Our Friendship",
                attender: ["alice", "bob"]
            }, function (err, match) {
                should.not.exist(err);
                should.exist(match);
                match.should.have.property('attender');
                match.attender.should.not.be.empty;
                user.matches_created.should.include(match._id);
                done();
            });
        });
    });

    it('should populate the user object', function (done) {
        logic.getUser("foo", function (err, user) {
            should.not.exist(err);
            should.exist(user);
            logic.getUserByOid(user._id, function (err, user) {
                should.exist(user);
                should.not.exist(err);
                user.matches_created.should.not.be.empty;
                user.matches_created[0].should.have.property('body',  "Long Life Our Friendship");
                done();
            });
        });
    });

    it('should make relation with alice and bob', function (done) {
        logic.getUser("alice", function (err, user) {
            should.not.exist(err);
            should.exist(user);
            user.matches_related.should.not.be.empty;
            user.matches_related[0].should.have.property('creator');
            logic.getUser(user.matches_related[0].creator, function (err, user) {
                should.not.exist(err);
                should.exist(user);
                user.should.have.property("uid", "foo");
                done();
            });
        });
    });

    it('should not set the match to approved after alice approve the match', function (done) {
        logic.getUser("alice", function (err, user) {
            should.not.exist(err);
            should.exist(user);
            user.matches_related.should.not.be.empty;
            var match = user.matches_related[0];
            match.should.not.have.property('approved', true);
            logic.approveMatch(user, match, "Jason is insane", function (err, match) {
                match.should.not.have.property('approved', true);
                done();
            });
        });
    });

    it('should set the match to approved after bob approve the match too', function (done) {
        logic.getUser("bob", function (err, user) {
            should.not.exist(err);
            should.exist(user);
            user.matches_related.should.not.be.empty;
            var match = user.matches_related[0];
            match.should.not.have.property('approved', true);
            match.should.not.have.property('approved_at');
            logic.approveMatch(user, match, "Alice is insane", function (err, match) {
                match.should.have.property('approved', true);
                match.should.have.property('approved_at');
                done();
            });
        });
    });

});
