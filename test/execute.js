
describe("Execute.js", function () {

    var when ,now, wrap;
    now = when = wrap = require('../lib/execute')('execute');

    var should = require('should');

    it('typeof an undefined variable should be undefined', function () {
        should.strictEqual(typeof undefined, "undefined");
    });

    it('should finish this test when 1s', function (done) {
        when('ok', done);
        setTimeout(function () {
            now('ok');
        }, 1000);
    });

    it('should call wrapped function with calling parameters', function (done) {
        wrap('ok', function (args) {
            should.ok(args);
            done();
        }, true)("foo");
    });

});
