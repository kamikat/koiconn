Koi Connect
===========

The name of the application comes from japanese こいコネクト(Love Connect).
The application is an social network entertainment application for the 
Renren open platform.

HOW TO
------

User access the application through the page of Renren, authorize the app.
Then system will list the user's friends out in bubbles.
By draging two friend into the frame, the two friend will be bound. 
A message will be sent to both of the friends.

After both of the friends say 'Yes' to the match, the originate user get
points for the match.

System will display a HOF of the matcher

Technical Information
---------------------

System use a MongoDB as persistence provider
Communication framework is Socket.IO
