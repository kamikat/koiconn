
all: static/js/templates.js

static/js/templates.js: templates/runtime/*
	./node_modules/clientjade/bin/clientjade templates/runtime/*.jade > static/js/templates.js
	
clean:
	rm static/js/templates.js

.PHONY: all

