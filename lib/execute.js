
// Execute.js helper to async API

var obtain = function (initial_state) {
    if(!initial_state) initial_state = 'unknown';
    var c_state = initial_state;
    var events = {};
    var execute = function (state, task, wrap) {
        if(wrap) {
            // Return a wrapper for original function
            return function () {
                var args = arguments;
                execute(state, function () {
                    task.apply(this, args);
                });
            };
        }
        if(!events[state])
            events[state] = []; 
        if(typeof task == 'function') {
            events[state].push(task);
        }else if(typeof state == 'string') {
            c_state = state;
        }
        while(events[c_state] && events[c_state].length)
            events[c_state].shift()();
    };
    return execute;
};

module.exports = obtain;
