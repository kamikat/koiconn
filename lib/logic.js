
var _ = require('underscore');
var Renren = require('../../node-renren');
var exe = require('./execute');

var koidb = require('./db');

koidb.connect('mongodb://localhost/koiconn');

// This code handle the main logic with Socket.IO

var renren_config = _({
    app_key:"e083e431649d43748447610c594e7ac5",
    app_secret:"f18b573c1b924896a392aeebfade538d",
    redirect_uri:"http://apps.renren.com/koiconn",
    api_group: []
});

var use = function (io) {

    var uid2socket = {};

    io.sockets.on('connection', function (socket) {

        var execute = exe();

        var user, api;

        var setUser = function (u) {
            user = u;
        };

        socket.on('token', function (data) {

            var cfg = renren_config.clone();
            cfg.access_token = data.access_token;
            cfg.expires_in = data.expire;

            api = new Renren(cfg);
            execute('apiready');

            api.users.getLoggedInUser({}, function (err, uid) {
                if(err) {
                    console.error('RenrenError', err);
                    console.error('ErroContext', cfg);
                    socket.emit('error', {
                        code: 1000, 
                        message: "Failed to obtain uid"
                    });
                    return;
                }

                koidb.getUser(uid.uid, function (err, user) {
                    var ok = function (err, user) {
                        setUser(user);
                        var u = {
                            uid: user.uid,
                            name: user.name,
                            avatar: user.avatar,
                            score: user.score
                        }
                        socket.emit('ready', u);
                        // Ugly - Emit online event...
                        chat.emit('online', u);
                        // Emit dynamics
                        uid2socket[uid.uid] = socket;
                        deliverDynamic(user.uid);
                    };
                    if(koidb.isUserProfileExpired(user)) {
                        // update the user profile in database
                        api.users.getInfo({
                            uids: uid.uid,
                            fields: "uid,name,tinyurl,headurl,mainurl"
                        }, function (err, info) {
                            if(err) {
                                console.error('RenrenError', err);
                                console.error('ErroContext', uid);
                                if(user.name === undefined) {
                                    socket.emit('error', {
                                        code: 1001, 
                                        message: "Failed to obtain user information"
                                    });
                                }else{
                                    ok(err, user);
                                }
                                return;
                            }
                            koidb.updateUserProfile(user, {
                                name: info[0].name,
                                avatar: [info[0].tinyurl, info[0].headurl, info[0].mainurl]
                            }, ok);
                        });
                    }else{
                        ok(err, user);
                    }
                });
            });
        });

        socket.on('disconnect', function () {
            if(user)
                uid2socket[user.uid] = undefined;
        });

        var chat = io.of('/chat');

        chat.on('connection', function (socket) {

            socket.on('chat', function (data) {
                chat.emit('chat', {
                    user: user,
                    time: new Date(),
                    msg: data
                });
            });

            socket.on('disconnect', function () {
                socket.broadcast.emit('offline', user);
            });

        });

        var deliverDynamic = (function () {
            var dynamics = {};
            return function (uid, data) {
                if(!dynamics[uid]) dynamics[uid] = [];
                if(data)
                    dynamics[uid].push(data);
                if(uid2socket[uid] && uid2socket[uid].length) {
                    uid2socket[uid].emit('dynamic', dynamics[uid]);
                    dynamics[uid] = [];
                }
            };
        })();

        var notification_content = function (orz, katas) {
            return '<xn:name uid="'+orz+'" linked="true"/>认为你和'+
                '<xn:name uid="'+katas.join(',')+'" linked="true"/>很般配哦！'+
                '<a href="http://apps.renren.com/koiconn">查看详情</a>';
        };

        var handleRenren = function () {
            console.log(JSON.stringify(arguments));
        };

        var scorelevel = [10, 40, 50, 30];

        var scoreUp = function (uid, level) {
            koidb.getUser(uid, function(err, user) {
                koidb.addToUserScore(user, scorelevel[level], function (err, user) {
                    if(uid2socket[uid])
                        uid2socket[uid].emit('score', user.score);
                });
            });
        };

        var koi = io.of('/koi');

        koi.on('connection', function (socket) {

            socket.on('friends', function () {
                execute('apiready', function () {
                    api.friends.get({}, function (err, uids) {
                        if(err) return handleRenren(err);
                        api.users.getInfo({
                            uids: uids.join(","),
                            fields: "uid,name,tinyurl,mainurl"
                        }, function (err, users) {
                            if(err) return handleRenren(err);
                            socket.emit('friends', users);
                        });
                    });
                });
            });

            socket.emit('rank', 1);

            socket.on('create', function (data) {
                // data:
                //  {
                //   body: String,
                //   attender: [uid, uid, ...]
                //  }
                koidb.newMatch(user, data, function (err, match) {
                    if(err) {
                        socket.emit('error', {
                            code: 2001, 
                            message: "Failed to create match"
                        });
                        return handleRenren(err);
                    }
                    match.attender.forEach(function (kata) {
                        api.notifications.send({
                            to_ids: kata.uid,
                            notification: notification_content(
                                user.uid, _(match.attender).chain()
                                .filter(function (x) {
                                    return x.uid != kata.uid;
                                }).pluck('uid').value())
                        }, function (err, data) {
                            if(err) return handleRenren(err);
                        });
                        deliverDynamic(kata.uid, {type: 'match', ref: match._id});
                    });
                    scoreUp(user.uid, 0);
                });
            });

            socket.on('accept', function (data) {
                // data: 
                //  {
                //   mid: id,
                //   body: String
                //  }
                var handleApprove = function () {
                    socket.emit('error', {
                        code: 2002, 
                        message: "Failed to approve match"
                    });
                }
                koidb.getMatch(data.id, function (err, match) {
                    if(err) return handleApprove(err);
                    koidb.approveMatch(user, match, data.body, function (err, match) {
                        if(err) return handleApprove(err);
                        if(match.approved) {
                            scoreUp(match.creator, 2)
                            // TODO Send Notification?
                            deliverDynamic(match.creator, {type: 'matched', ref: match._id});
                            match.attender.forEach(function (u) {
                                if(u.uid != user.uid) {
                                    deliverDynamic(u.id, {type: 'matched', ref: match._id});
                                }
                            });
                        }else{
                            scoreUp(match.creator, 1)
                            // TODO Send Notification?
                            // TODO Send Dynamic
                            deliverDynamic(match.creator, {type: 'match-progress', ref: match._id});
                            match.attender.forEach(function (u) {
                                if(u.uid != user.uid) {
                                    deliverDynamic(u.id, {type: 'match-progress', ref: match._id});
                                }
                            });
                        }
                        scoreUp(user.uid, 3);
                    });
                });
            });

            socket.on('match', function (data, fn) {
                koidb.getMatch(data, function (err, match) {
                    if(err) return fn('err');
                    fn(match);
                });
            });

            socket.on('who', function (data) {
                // data = uid
                koidb.getUser(data, function (err, user) {
                    var ok = function (err, user) {
                        var u = {
                            uid: user.uid,
                            name: user.name,
                            avatar: user.avatar
                        }
                        socket.emit('he is', u);
                    };
                    if(koidb.isUserProfileExpired(user)) {
                        // update the user profile in database
                        api.users.getInfo({
                            uids: uid.uid,
                            fields: "uid,name,tinyurl,headurl,mainurl"
                        }, function (err, info) {
                            if(err) {
                                console.error('RenrenError', err);
                                console.error('ErroContext', uid);
                                if(user.name === undefined) {
                                    socket.emit('error', {
                                        code: 2001, 
                                        message: "Failed to obtain user information"
                                    });
                                }else{
                                    ok(err, user);
                                }
                                return;
                            }
                            koidb.updateUserProfile(user, {
                                name: info[0].name,
                                avatar: [info[0].tinyurl, info[0].headurl, info[0].mainurl]
                            }, ok);
                        });
                    }else{
                        ok(err, user);
                    }
                });

            });

        });


    });

    return io;
};

module.exports = {
    use: use
};
