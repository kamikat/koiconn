
var exp = require('express');
var path = require('path');

var midware = {
    less: require('less-middleware')
};

var app = exp();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);

app.configure(function() {

    // Jade template path configuration
    app.set('views', path.resolve(__dirname + '/../templates'));

    // Middleware configuration
    app.use(exp.bodyParser());
    app.use(exp.cookieParser());
    app.use(app.router);
    app.use(midware.less({
        src: path.resolve(__dirname + '/../less'),
        dest: path.resolve(__dirname + '/../static/css'),
        prefix: '/css', 
        compress: true
    }));

    // Static path configuration
    app.use(exp.static(path.resolve(__dirname + '/../static')));

});

require('./logic').use(io);

app.get('/', function(req, res) {
    res.render('index.jade');
});

// Initialization completed

module.exports = server;
