var should = require('should');

var when, now;
now = when = require('./execute')();

var safe = function (fn) {
    return when('initialized', fn, true);
};

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var conn = function (connString) {

    mongoose.connect(connString);

    var db = mongoose.connection;

    db.on('error', console.error.bind(console, 'connection error:'));

    db.once('open', function () {

        // Schema Definition Section

        var userSchema = mongoose.Schema({
            uid: String,

            // User Information Field
            name: String,
            avatar: [String],
            last_updated: Date,

            // Information Related to Application
            score: {type: Number, default: 0},
            matches_created: [{type: Schema.Types.ObjectId, ref: 'Match'}],
            matches_related: [{type: Schema.Types.ObjectId, ref: 'Match'}]
        });

        var matchSchema = mongoose.Schema({
            creator: String,
            body: String,
            attender: [mongoose.Schema({
                uid: String,
                body: String,
                approved: {type: Boolean, default: false},
                approved_at: Date
            })],
            approved: {type: Boolean, default: false}, 
            approved_at: Date,
            created_at: {type: Date, default: Date.now}
        });

        // End Definition of Schema

        var _User = mongoose.model('User', userSchema);
        var _Match = mongoose.model('Match', matchSchema);

        // Distribute Variable to Variables
        logic.Types.User = User = _User;
        logic.Types.Match = Match = _Match;

        // Issue new Executable state
        now('initialized');

    });

};

var User, Match;

var logic = {
    mongoose: mongoose,
    connect: conn,
    close: function (callback) {
        mongoose.connection.close(callback);
        now('closed');
    },
    Types: {},
    getUserByOid: safe(function (id, callback) {
        User.findOne({_id: id})
        .populate('matches_created', null, null, { sort: [['approved', 1], ['approved_at', -1], ['created_at', -1]] })
        .populate('matches_related', null, null, { sort: [['approved', 1], ['approved_at', -1], ['created_at', -1]] })
        .exec(callback);
    }),
    getUser: safe(function (uid, callback) {
        User.findOne({uid: uid})
        .populate('matches_created', null, null, { sort: [['approved', 1], ['approved_at', -1], ['created_at', -1]] })
        .populate('matches_related', null, null, { sort: [['approved', 1], ['approved_at', -1], ['created_at', -1]] })
        .exec(function (err, user) {
            if(err) return callback(err);
            if(user === null) {
                // No such user
                logic.newUser({uid: uid}, callback);
            }else{
                callback(err, user);
            }
        });
    }),
    getMatchByOid: safe(function (_id, callback) {
        Match.findOne({_id: _id})
        .exec(callback);
    }),
    getMatch: safe(function (id, callback) {
        logic.getMatchByOid(mongoose.Types.ObjectId(id), callback);
    }),
    newUser: safe(function (data, callback) {
        // data:
        //  {
        //   uid: String,
        //  [--Optional--]
        //  |name: String,
        //  |avatar: [
        //  | String, // tiny
        //  | String, // normal
        //  | String  // large
        //  |]
        //  }
        var user = new User(data);
        if(data.name !== undefined) {
            user.last_updated = Date.now();
        }
        user.save(callback);
    }),
    isUserProfileExpired: function (user) {
        // Always Update User Profile
        return true;
    },
    updateUserProfile: safe(function (user, data, callback) {
        user.name = data.name;
        user.avatar = data.avatar;
        user.last_updated = Date.now();
        user.save(callback);
    }),
    updateUserScore: safe(function (user, score, callback) {
        user.score = score;
        user.save(callback);
    }),
    addToUserScore: safe(function (user, delta, callback) {
        logic.updateUserScore(user, user.score + delta, callback);
    }),
    newMatch: safe(function (user, data, callback) {
        // data :
        //  {
        //   body: String,
        //   attender: [uid, uid, ...]
        //  }
        var match = new Match({
            creator: user.uid,
            body: data.body,
            attender: [],
        });
        data.attender.forEach(function (uid) {
            match.attender.push({uid: uid});
        });
        match.save(function (err, match) {
            if(err) return callback(err);

            // Some assertion for debug
            should.exist(match);
            match.should.have.property('_id');

            user.matches_created.push(match._id);
            user.save(function (err) { 
                if(err) return callback(err);
                var c = match.attender.length;
                match.attender.forEach(function (u) {
                    logic.getUser(u.uid, function (err, user) {
                        if(err) return callback(err);
                        user.matches_related.push(match._id);
                        user.save(function (e) { 
                            c--;
                            if(!c) callback(err, match);
                        });
                    });
                });
            });
        });
    }),
    approveMatch: safe(function (user, match, data, callback) {
        match.should.have.property('attender');
        var approved = true;
        match.attender.forEach(function (u) {
            if(u.uid == user.uid) {
                u.body = data;
                u.approved = true;
            }
            if(!u.approved)
                approved = false;
        });
        if(approved) {
            match.approved = true;
            match.approved_at = Date.now();
        }
        match.save(callback);
    }),
};

module.exports = logic;
