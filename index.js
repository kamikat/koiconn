#!/usr/bin/env node

(function () {

    var server = require('./lib/server.js');

    server.listen(6067);
    console.log('Listen on 6067.');

})();
