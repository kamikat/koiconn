(function () {

    window.appId = "222907";

    var uiOpts = {
        url: "http://graph.renren.com/oauth/authorize",
        display: "iframe",
        style: {top: 50, left:150, width:480, height:450},
        params: {
            "response_type": "token",
            "client_id": appId,
            "scope": "send_invitation publish_share send_request"
        },
        onSuccess: function(r){
            logic.splashReady();
            logic.connect(r.access_token, r.expires_in);
        },
        onFailure: function(r){} 
    };
    Renren.ui(uiOpts);

})();
