// This module store common utilities

var getTemplate = function (name, data) {
    return jade.templates[name](data);
}

var preload = function (arrayOfImages, callback) {
    var count = arrayOfImages.length;
    $(arrayOfImages).each(function() {
        var img = $('<img/>');
        img.attr('src', this).load(function () {
            count--;
            if(count == 0) {
                callback(arrayOfImages);
            }
        });
    });
}

// $('jq_path').on(transitionEndEvent, function () { ... })
var transitionEndEvent = (function whichTransitionEvent(){
    return 'transitionend webkitTransitionEnd oTransitionEnd msTransitionEnd transitionEnd';
})();

