
var logic = {};

window.logic = logic;

(function ($) {

    var page_state = 'init';
    var events = {
        init: [],
        auth: [],
        splash: [],
        chatroom: []
    };

    var execute = function (state, task) {
        if(!events[state])
            events[state] = [];
        if(typeof task == 'function') {
            events[state].push(task);
        }else if(typeof state == 'string') {
            page_state = state;
        }
        while(typeof events[page_state] && events[page_state].length)
            events[page_state].shift()();
    };

    var _onload = window.onload;

    window.onload = function () {
        execute('auth');
        if(_onload) _onload();
    };

    var socketio_cfg = {
        transports: ['websocket', 'htmlfile', 'xhr-multipart', 'xhr-polling', 'jsonp-polling']
    };

    logic.splashReady = function () {
        execute('auth', function () {
            $('#splash').addClass('splash')
            .one(transitionEndEvent, function () {
                execute('splash');
            });
        });
    };
    
    var delaywrapper = function (delay) {
        return function(fn) {_.delay(fn, delay);};
    };

    logic.gotoChatRoom = function () {
        preload([
            '/img/blur.jpg'
        ], _(function () {
            $('<div class="blur"/>').prependTo('#splash');
            execute('splash', function () {
                $('#splash').addClass('chatroom')
                .one(transitionEndEvent, _(function () {
                    $('#splash .background').remove();
                    $('#splash [class^="status"]').remove();
                    $('#splash .spinner').remove();
                    $(getTemplate('chatboard')).appendTo('#splash');
                    execute('pre-chatroom');
                }).wrap(_.defer));
            });
        }).wrap(delaywrapper(500)));
    };

    logic.gotoKoiRoom = function () {
        preload([
            '/img/blur.jpg'
        ], (function () {
            $('<div class="blur"/>').prependTo('#splash');
            execute('splash', function () {
                $('#splash').addClass('koiconn')
                .one(transitionEndEvent, _(function () {
                    $('#splash .background').remove();
                    $('#splash [class^="status"]').remove();
                    $('#splash [class^="hr"]').remove();
                    $('#splash .sub-title-en').remove();
                    $('#splash .spinner').remove();
                    $(getTemplate('koiboard')).appendTo('#splash');
                    execute('pre-koi');
                }).wrap(_.defer));
            });
        }));
    };

    execute('pre-koi', _(function () {
        $('#koi').addClass('wosuru')
        .one(transitionEndEvent, function () {
            execute('jounetsu');
        });
        $('#match').click(function () {
            var attender = [];
            $('#koi-candidates .friend.selected').each(function (i, e) {
                attender.push($(e).attr('data-uid'));
            });
            console.log(attender);
            logic.sockets.koi.emit('create', {
                body: $('#match-body').val(),
                attender: attender
            });
            $('#match-body').val('');
            $('#koi-candidates .friend.selected').removeClass('selected');
            updateTopPanel();
        });
        $('#koi-panel form').submit(function () {
            // TODO Navigate to friend here
            return false;
        });
        // Configure search of friend
        $('#koi-panel #search')[0].oninput = (function () {
            var condition = $(this).val();
            $('#koi-candidates .friend').removeClass('highlight');
            if(!condition.length) {
                $('#koi').removeClass('search');
                $(this).parent().removeClass('error');
                return;
            }
            $('#koi').addClass('search');
            var found = false;
            $('#koi-candidates .friend').each(function (i, v) {
                var friend = $(v);
                if(friend.attr('data-name').indexOf(condition) >= 0) {
                    friend.addClass('highlight');
                    found = true;
                }
            });
            if(found) {
                $(this).parent().removeClass('error');
            }else{
                $(this).parent().addClass('error');
            }
        });
        displayUser();
    }).wrap(delaywrapper(100)));

    execute('pre-chatroom', _(function () {
        $('#chat').addClass('chatroom')
        .one(transitionEndEvent, function () {
            execute('chatroom');
        });
        $('#chat-panel form').submit(function () {
            var text = $('#chat-message').val();
            if(text && text.length > 0) {
                logic.chat(text);
                $('#chat-message').val("");
            }
            return false;
        });
    }).wrap(delaywrapper(100)));

    logic.reconnect = function (access_token, expire_in) {
        if(logic.sockets.socket)
            logic.sockets.socket.disconnect();
        if(logic.sockets.chat)
            logic.sockets.chat.disconnect();
        logic.connect(access_token, expire_in);
    };

    logic.sockets = {};

    logic.systemuser = {
        name: "系统消息",
        avatar: "",
        uid: 0
    };

    logic.connect_chat = function () {

        var chat = io.connect('http://koiconn.shinohane.com:6067/chat', socketio_cfg);

        // Chatroom Events
        chat.on('connect', function () {

            addMessage({
                user: {
                    name: logic.systemuser.name,
                    avatar: logic.user.avatar,
                    uid: logic.user.uid
                },
                time: new Date(),
                msg: "已连接到产品反馈频道，当前系统正在建设中..." +
                    "可以在本频道讨论本应用相关的话题以及进行用户反馈\n" +
                    "下面介绍基本使用方法\n" + 
                    "1） 在下面输入框中输入需要发送的文本\n" + 
                    "2） 点击“发送”按钮或者回车键发送消息\n" + 
                    "3） 使用clear命令清空消息框\n" + 
                    "注意：本频道为公共频道"
            });

            chat.on('chat', function (data) {
                data.time = new Date(data.time);
                addMessage(data);
            });
            chat.on('online', function (user) {
                addMessage({
                    user: {
                        name: logic.systemuser.name,
                        avatar: user.avatar,
                        uid: user.uid
                    },
                    time: new Date(),
                    msg: user.name + "加入了频道"
                });
            });
            chat.on('offline', function (user) {
                addMessage({
                    user: {
                        name: logic.systemuser.name,
                        avatar: user.avatar,
                        uid: user.uid
                    },
                    time: new Date(),
                    msg: user.name + "离开了频道"
                });
            });
            chat.on('disconnect', function () {
                logic.sockets.chat = undefined;
                addMessage({
                    user: {
                        name: logic.systemuser.name,
                        avatar: logic.user.avatar,
                        uid: logic.user.uid
                    },
                    time: new Date(),
                    msg: "与服务器的连接已断开，请确认服务器是否还活着……"
                });
            });
            logic.sockets.chat = chat;
            logic.gotoChatRoom();
        });

    };

    logic.connect_koi = function () {
        var koi = io.connect('http://koiconn.shinohane.com:6067/koi', socketio_cfg);
        koi.on('connect', function () {
            // koi.emit('friends');
            koi.on('dynamic', function (data) {
                data.forEach(function (dyn) {

                });
            });
            koi.on('friends', function (friends) {
                friends.forEach(addFriend);
            });
            koi.on('rank', function (rank) {
                execute('jounetsu', function () {
                    $('.rank').text(rank).addClass('show');
                });
            });
            koi.on('score', function (score) {
                logic.user.score = score;
                $('#user-panel .score').text(logic.user.score);
            });
            koi.on('disconnect', function () {
                logic.sockets.koi = undefined;
            });
            logic.sockets.koi = koi;
            logic.gotoKoiRoom();
        });
    };

    var displayUser = function () {
        $('#avatar').attr('src', logic.user.avatar[1]);
        $('#user-panel .user').text(logic.user.name + ' (' + logic.user.uid + ')');
        $('#user-panel .score').text(logic.user.score);
    };

    logic.connect = function (access_token, expire_in) {
        var socket = io.connect('http://koiconn.shinohane.com:6067', socketio_cfg);
        socket.on('connect', function () {
            socket.emit('token', {
                access_token: access_token,
                expire: expire_in
            });
            socket.on('ready', function (data) {
                logic.user = data;
                logic.connect_koi();
            });
            socket.on('expire', function () {
                console.log('access_token expired');
            });
            socket.on('disconnect', function () {
                logic.sockets.socket = undefined;
            });
            socket.on('error', function (err) {
                if(err.code < 1999) {
                    // Fatal Error
                    logic.reconnect(access_token, expire_in);
                }
            });
            logic.sockets.socket = socket;
        });
    };

    var updateTopPanel = function () {
        var $selected = $('.friend.selected');
        $('#koi-upper .friend').remove();
        var stage = $selected.length;
        if(stage == 1) $('#koi-upper').removeClass('two').addClass('one');
        if(stage == 2) $('#koi-upper').removeClass('one').addClass('two');
        if(stage == 0) $('#koi-upper').removeClass('one').removeClass('two');
        $selected.each(function (i, e) {
            var data = uid2user[$(e).attr('data-uid')];
            $(getTemplate('friend', {
                name: data.name, 
                avatar: data.mainurl,
                uid: data.uid
            })).prependTo('#koi-upper')
            .click(function () {
                var $this = $(this);
                $('#koi-candidates .friend[data-uid="' + $this.attr('data-uid') + '"]').removeClass('selected');
                $this.remove();
                if($('#koi-upper').hasClass('two')) {
                    $('#koi-upper').removeClass('two').addClass('one');
                }else if($('#koi-upper').hasClass('one')) {
                    $('#koi-upper').removeClass('one').removeClass('two');
                }
            });
        });
    };

    var uid2user = {};
    
    var addFriend = function (data) {
        execute('jounetsu', function () {
            uid2user[data.uid] = data;
            $(getTemplate('friend', {
                name: data.name, 
                avatar: data.tinyurl,
                uid: data.uid
            })).appendTo('#koi-candidates')
            .click(function () {
                if($('.friend.selected').length == 2 && !$(this).hasClass('selected')) return;
                $(this).toggleClass('selected');
                updateTopPanel();
            });
        });
    };

    var addMessage = function (data) {
        execute('chatroom', function () {
            var $msgs = $('#chat-messages');
            var max_scroll = $msgs[0].scrollHeight - $msgs.height();
            $(getTemplate('chatmsg', data)).appendTo('#chat-messages');
            if($msgs.scrollTop() == max_scroll) {
                $msgs.scrollTop($msgs[0].scrollHeight - $msgs.height());
            }
        });
    };

    logic.chat = function (msg) {
        if(msg == 'clear') {
            execute('chatroom', function () {
                $('#chat-messages').empty();
            });
            return;
        }
        if(logic.sockets.chat)
            logic.sockets.chat.emit('chat', msg);
    };

})(window.jQuery);
